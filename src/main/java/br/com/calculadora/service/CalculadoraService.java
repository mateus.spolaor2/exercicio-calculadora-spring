package br.com.calculadora.service;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.model.Calculadora;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora){
        double resultado = 0;
        for (Integer numero: calculadora.getNumeros()
             ) {
            resultado += numero;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);

        return respostaDTO;
    }

    public RespostaDTO subtrair(Calculadora calculadora){
        double resultado = 0;
        resultado = calculadora.getNumeros().get(0) - calculadora.getNumeros().get(1)  ;
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO multiplicar(Calculadora calculadora){
        double resultado = 1d;
        for (Integer numero: calculadora.getNumeros()
        ) {
            resultado *= numero;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);

        return respostaDTO;
    }

    public RespostaDTO dividir(Calculadora calculadora){
        double resultado = 1d;
        List<Double> nrosParaCalculo = converteParaDouble(calculadora);
        resultado = nrosParaCalculo.get(0) / nrosParaCalculo.get(1);
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public List<Double> converteParaDouble(Calculadora calculadora) {
        List<Double> listaConvertida = new ArrayList<>();
        for (Integer numero: calculadora.getNumeros()) {
            double converter = numero;
            listaConvertida.add(converter);
        }
        return listaConvertida;
    }
}
