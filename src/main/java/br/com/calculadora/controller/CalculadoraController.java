package br.com.calculadora.controller;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.model.Calculadora;
import br.com.calculadora.service.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário o envio de mais de um número");
        }
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().get(0) < calculadora.getNumeros().get(1)){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "É necessário que o primeiro número seja maior ou igual que o segundo"
            );
        } else if(calculadora.getNumeros().size() != 2){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Esta API aceita apenas dois algarismos."
            );
        } else {
            return calculadoraService.subtrair(calculadora);
        }
    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário o envio de mais de um número");
        }
        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public RespostaDTO dividir(@RequestBody Calculadora calculadora){
        if(calculadora.getNumeros().get(0) < calculadora.getNumeros().get(1)){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "É necessário que o primeiro número seja maior ou igual que o segundo"
            );
        } else if(calculadora.getNumeros().size() != 2){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    "Esta API aceita apenas dois algarismos."
            );
        } else {
            return calculadoraService.dividir(calculadora);
        }
    }

}
